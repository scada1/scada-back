import {
  createFileReceipt,
  createFileImport,
  createFileMatch,
  getDataByDay 
} from '../repository/scada';

export const createFileReceiptMapper = (res, params) => {
  const filename = params.filename;
  const type = params.type;
  const date = '2020-05-27';
  createFileReceipt(res, filename, type, date);
};

export const createFileImportMapper = (res, params) => {
  params.date = '2020-05-27';

  createFileImport(res, params);
};

export const createFileMatchMapper = (res, params) => {
  params.date = '2020-05-27';

  createFileMatch(res, params);
};

export const getDataByDayMapper = (res, date) => {

  getDataByDay(res, date);
};

import { validate } from './core';

export const createFileReceiptValidator = req => {
  const validators = [
    {
      field: 'filename',
      predicate: param => true,
      predicateErrorMsg: 'filename must be a string'
    },
    {
      field: 'type',
      predicate: param => true,
      predicateErrorMsg: 'type must be in [SAFE, TC40, NXS]'
    }
  ]

  return validate(validators, req.body);
};

export const createFileImportValidator = req => {
  const validators = [
    {
      field: 'filename',
      predicate: param => true,
      predicateErrorMsg: 'filename must be a string'
    },
    {
      field: 'type',
      predicate: param => true,
      predicateErrorMsg: 'type must be in [SAFE, TC40, NXS]'
    },
    {
      field: 'errors',
      predicate: param => true,
      predicateErrorMsg: 'type must be in [SAFE, TC40, NXS]'
    },
    {
      field: 'lineRead',
      predicate: param => true,
      predicateErrorMsg: 'lineRead must be a number'
    },
    {
      field: 'lineImported',
      predicate: param => true,
      predicateErrorMsg: 'lineImported must be a number'
    },
    {
      field: 'lineFailed',
      predicate: param => true,
      predicateErrorMsg: 'lineFailed must be a number'
    },
  ]

  return validate(validators, req.body);
};

export const createFileMatchValidator = req => {
  const validators = [
    {
      field: 'type',
      predicate: param => true,
      predicateErrorMsg: 'type must be in [SAFE, TC40, NXS]'
    },
    {
      field: 'errors',
      predicate: param => true,
      predicateErrorMsg: 'type must be in [SAFE, TC40, NXS]'
    },
    {
      field: 'lineAttempted',
      predicate: param => true,
      predicateErrorMsg: 'lineAttempted must be a number'
    },
    {
      field: 'lineMatched',
      predicate: param => true,
      predicateErrorMsg: 'lineMatched must be a number'
    },
    {
      field: 'lineFailed',
      predicate: param => true,
      predicateErrorMsg: 'lineFailed must be a number'
    },
  ]

  return validate(validators, req.body);
};

export const getDataByDayValidator = req => {
  const validators = [
    {
      field: 'date',
      predicate: param => true,
      predicateErrorMsg: 'date must be in ...'
    }
  ];

  return validate(validators, req.params);
};

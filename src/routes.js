import express from 'express'
import {
  createFileReceiptAction,
  createFileImportAction,
  createFileMatchAction,
  getDataByDayAction
} from './controller/scada';
let router = express.Router();

router.get('/data/:date', getDataByDayAction);
router.post('/receipt', createFileReceiptAction);
router.post('/import', createFileImportAction);
router.post('/match', createFileMatchAction);

export default router;

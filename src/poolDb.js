//EXAMPLE
import mongoose from 'mongoose';
import dotenv from 'dotenv';

dotenv.config();

const host = process.env.MONGO_HOST;
const user = process.env.MONGO_USER;
const password = process.env.MONGO_PASSWORD;
const database = process.env.MONGO_DATABASE;

// Ces options sont recommandées par mLab pour une connexion à la base
let options = { server: { socketOptions: { keepAlive: 300000, connectTimeoutMS: 30000 } },
    replset: { socketOptions: { keepAlive: 300000, connectTimeoutMS : 30000 } } };

//URL de notre base
let urlmongo = `mongodb+srv://${user}:${password}@${host}/${database}?retryWrites=true&w=majority`;

// Nous connectons l'API à notre base de données
mongoose.connect(urlmongo, options);

let db = mongoose.connection;

export default {
    "dbScada": db,
}

import mongoose from 'mongoose';

const receiptSchema = mongoose.Schema({
  filename: {
    type: String,
    required: true
  },
  type: {
    type: String,
    required: true
  },
  date: {
    type: String,
    required: true
  }
});

const Receipt = mongoose.model('receipt', receiptSchema);

export default Receipt;

import mongoose from 'mongoose';

const matchSchema = mongoose.Schema({
  type: {
    type: String,
    required: true
  },
  date: {
    type: String,
    required: true
  },
  script_errors: {
    type: Array,
    required: true,
  },
  lineAttempted: {
    type: Number,
    required: true,
  },
  lineMatched: {
    type: Number,
    required: true,
  },
  lineFailed: {
    type: Array,
    required: true,
  }
});

const Match = mongoose.model('match', matchSchema);

export default Match;

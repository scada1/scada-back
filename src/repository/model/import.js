import mongoose from 'mongoose';

const importSchema = mongoose.Schema({
  filename: {
    type: String,
    required: true
  },
  type: {
    type: String,
    required: true
  },
  date: {
    type: String,
    required: true
  },
  script_errors: {
    type: Array,
    required: true,
  },
  lineRead: {
    type: Number,
    required: true,
  },
  lineImported: {
    type: Number,
    required: true,
  },
  lineFailed: {
    type: Array,
    required: true,
  }
});

const Imported = mongoose.model('import', importSchema);

export default Imported;

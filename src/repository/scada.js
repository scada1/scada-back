import { Receipt, Imported, Match } from './model';

export const createFileReceipt = (res, filename, type, date) => {
  let receipt = new Receipt();

  receipt.filename  = filename;
  receipt.type      = type;
  receipt.date      = date;

  receipt.save( err => {
    if (err)
      res.json(err);

    res.json({
      message: "Receipt created"
    });

  });

};

export const createFileImport = (res, params) => {
  let imported = new Imported();

  imported.filename            = params.filename;
  imported.type                = params.type;
  imported.date                = params.date;
  imported.script_errors       = params.errors;
  imported.lineRead            = params.lineRead;
  imported.lineImported        = params.lineImported;
  imported.lineFailed          = params.lineFailed;

  imported.save( err => {
    if (err)
      res.json(err);

    res.json({
      message: "Import created"
    });

  });
};

export const createFileMatch = (res, params) => {
  let match = new Match();

  match.type = params.type;
  match.lineAttempted = params.lineAttempted;
  match.lineMatched = params.lineMatched;
  match.lineFailed = params.lineFailed;
  match.date = params.date;

  match.save( err => {
    if (err)
      res.json(err);

    res.json({
      message: "Match created"
    });

    });
}

export const getDataByDay = (res, date) => {
  let results = {date: date};
  console.log(date);
  //CALLBACK HELL INCOMING
  Receipt.find({ date: date }, (err, receipts) => {
    if (err) {
      res.json({
        status: 502,
        message: err
      });
    }

    results.receipts = receipts;

    Imported.find({ date: date }, (err, imports) => {
      if (err) {
        res.json({
          status: 502,
          message: err
        });
      }
      results.imports = imports;

      Match.find({ date: date }, (err, matchs) => {
        if (err) {
          res.json({
            status: 502,
            message: err
          });
        }

        results.matchs = matchs;

        res.send({data: results})
      });
    });
  });

}

import {
  createFileReceiptValidator,
  createFileImportValidator,
  createFileMatchValidator,
  getDataByDayValidator
} from '../validator/scada';

import {
  createFileReceiptMapper,
  createFileImportMapper,
  createFileMatchMapper,
  getDataByDayMapper
} from '../datamapper/scada';

import { badRequest } from '../httpRequest';

export const createFileReceiptAction = (req, res) => {
  const errors = createFileReceiptValidator(req);

  if (errors.length > 0)
    badRequest(res, errors);
  else
    createFileReceiptMapper(res, req.body);
}


export const createFileImportAction = (req, res) => {
  const errors = createFileImportValidator(req);

  if (errors.length > 0)
    badRequest(res, errors);
  else
    createFileImportMapper(res, req.body);
}


export const createFileMatchAction = (req, res) => {
  const errors = createFileMatchValidator(req);

  if (errors.length > 0)
    badRequest(res, errors);
  else
    createFileMatchMapper(res, req.body);
}

export const getDataByDayAction = (req, res) => {
  const errors = getDataByDayValidator(req);

  if (errors.length > 0)
    badRequest(res, errors);
  else
    getDataByDayMapper(res, req.params.date);
}
